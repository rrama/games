# README #

Please note this repository's **code has not been updated since early 2014!**
This API is newer than [RramaGaming](https://bitbucket.org/rrama/rramagaming), but is still outdated.

## Content ##

In this repository is an API for easily coding mini-games into the game Minecraft. This is further extended by [RoundedGames](https://bitbucket.org/rrama/roundedgames) and [TeamGames](https://bitbucket.org/rrama/teamgames).
This includes features such as;

* In-game commands.
* Handling ingame events (e.g. players entering custom defined regions).
* Loading random maps to play.
* Region definitions.
* A shop with purchasable items, currency is held via SQL.
* A rank system.
* Achievements.
* Referees.
* And much more!

## Author ##
[rrama](https://bitbucket.org/rrama/) (Ben Durrans).