package com.derplin.games.achievements;

import com.derplin.games.Games;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Represents an awardable {@link Achievement}.
 */
public class Achievement {

    private final String name;
    private final String pluginName;
    private final String sqlName;

    /**
     * {@link Achievement} constructor that the others point to.
     *
     * @param pluginName name of the {@link Plugin} the {@link Achievement} belongs to
     *                   or "Core" for core {@link Achievement}s.
     * @param name       the {@link Achievement}'s name.
     */
    private Achievement(String pluginName, String name) {
        this.name = name;
        this.pluginName = pluginName;
        sqlName = (pluginName + "_" + name).toLowerCase().replaceAll(" ", "_");
    }

    /**
     * Used by core {@link Achievement}s only.
     *
     * @param name the {@link Achievement}'s name.
     */
    protected Achievement(String name) {
        this("Core", name);
    }

    /**
     * {@link Achievement} constructor.
     *
     * @param plugin the owning {@link Plugin} the {@link Achievement} belongs to.
     * @param name   {@link Achievement} name.
     */
    public Achievement(Plugin plugin, String name) {
        this(plugin.getName(), name);
    }

    /**
     * Returns the name of the {@link Achievement}.
     *
     * @return the name of the {@link Achievement}.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the name of the {@link Plugin} that created this {@link Achievement}.
     * Can return "Core" if the {@link Achievement} is a core {@link Achievement}.
     *
     * @return the name of the {@link Plugin} that created this {@link Achievement}.
     */
    public String getOwningPlugin() {
        return pluginName;
    }

    /**
     * Returns the name used in the SQLite database.
     *
     * @return the name used in the SQLite database.
     */
    public String getSQLName() {
        return sqlName;
    }

    /**
     * Returns the message that will be broadcasted saying the {@link Player} has got the {@link Achievement}.
     * The message will also contain the {@link Player}'s name which is provided in the argument.
     *
     * @param pn The {@link Player}'s name that the {@link Achievement} is being awarded to.
     * @return the message that will be broadcasted saying the {@link Player} has got the {@link Achievement}.
     */
    public String getAwardMessage(String pn) {
        return ChatColor.GREEN + pn + " just unlocked " + ChatColor.WHITE + getName() + ChatColor.GREEN + "!";
    }
    
    /**
     * Returns if the {@link Player} has this {@link Achievement}.
     * 
     * @param player the {@link Player} to check.
     * @return {@code true} if the {@link Player} has this {@link Achievement}.
     * @throws Exception on any {@link SQLExceptions} and SQL errors.
     */
    public boolean hasAchievement(Player player) throws Exception {
        return hasAchievement(player.getName());
    }
    
    /**
     * Returns if the {@link Player} has this {@link Achievement}.
     * 
     * @param playerName the name of the {@link Player} to check.
     * @return {@code true} if the {@link Player} has this {@link Achievement}.
     * @throws Exception on any {@link SQLExceptions} and SQL errors.
     */
    public boolean hasAchievement(String playerName) throws Exception {
        return Games.getGamesDatabase().hasAchievement(playerName, getSQLName());
    }
    
    /**
     * Awards the {@link Player} this {@link Achievement}s and broadcasts the {@link Achievement} get message.
     * 
     * Safety checks for if the {@link Player} already has the achievement.
     * @param player the {@link Player} to award this {@link Achievement} to.
     */
    public void awardAchievement(Player player) {
        awardAchievement(player.getName());
    }
    
    /**
     * Awards the {@link Player} this {@link Achievement}s and broadcasts the {@link Achievement} get message.
     * 
     * Safety checks for if the {@link Player} already has the achievement.
     * @param playerName the name of the {@link Player} to award this {@link Achievement} to.
     */
    public void awardAchievement(String playerName) {
        try {
            if (hasAchievement(playerName)) return;
        } catch (Exception ex) {
            return;
        }
        if (!Games.getGamesDatabase().awardAchievement(playerName, getSQLName())) return;
        Bukkit.broadcastMessage(getAwardMessage(playerName));
    }

}