package com.derplin.games.achievements;

/**
 * The Achievement gotten for using /Store.
 */
public class StoreAchieve extends Achievement {

    /**
     * The default constructor for the {@link StoreAchieve}.
     */
    public StoreAchieve() {
        super("Store");
    }
    
}