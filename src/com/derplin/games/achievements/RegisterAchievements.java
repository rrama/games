package com.derplin.games.achievements;

import com.derplin.games.Games;
import java.util.Collection;
import java.util.HashMap;
import org.bukkit.plugin.Plugin;

/**
 * The class where you can register {@link Achievement}s and that stores {@link Achievement}s.
 */
public class RegisterAchievements {
    
    private static final HashMap<String, Achievement> allAchievements = new HashMap<>();
    
    /**
     * Calls the registering of all the {@link Achievement}s.
     */
    public static void register() {
        registerAchievement(new StoreAchieve());
    }
    
    /**
     * Adds the {@link Achievement} to list and makes sure it has a column in the database.
     * @param achievement the {@link Achievement} to register.
     */
    public static void registerAchievement(Achievement achievement) {
        allAchievements.put(achievement.getSQLName(), achievement);
        Games.getGamesDatabase().makeAchievementColumn(achievement.getSQLName());
    }
    
    /**
     * Returns all the {@link Achievement}s from all the {@link Plugin}s (includes core).
     * @return all the {@link Achievement}s from all the {@link Plugin}s (includes core).
     */
    public static Collection<Achievement> getAchievements() {
        return allAchievements.values();
    }
    
    /**
     * Gets the {@link Achievement} that has an SQL column name matching <code>name</code>.
     * @param name the SQL column name of the {@link Achievement}.
     * @return the {@link Achievement} that has an SQL column name matching <code>name</code> or {@code null}.
     */
    public static Achievement getAchievementBySQLName(String name) {
        return allAchievements.get(name);
    }
    
    /**
     * Gets the {@link Achievement} with the matching parameters.
     * @param plugin the owning {@link Plugin} of the {@link Achievement}.
     * @param name the name of the {@link Achievement}.
     * @return the {@link Achievement} that matches the parameters or {@code null}.
     */
    public static Achievement getAchievementByName(Plugin plugin, String name) {
        return getAchievementBySQLName((plugin.getName() + "_" + name).toLowerCase().replaceAll(" ", "_"));
    }
    
}