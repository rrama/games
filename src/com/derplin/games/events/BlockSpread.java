package com.derplin.games.events;

import java.util.ArrayList;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockSpreadEvent;

/**
 * A {@link Listener} that can be registered and unregistered by a game.
 * This {@link Listener} stops block in <code>nonspread</code> from spreading.
 * The game {@link Plugin} should first set <code>nonspread</code>.
 */
public class BlockSpread implements Listener {
    
    public static final ArrayList<Material> nonspread = new ArrayList<>();
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockSpread(BlockSpreadEvent event) {
        if (nonspread.contains(event.getBlock().getType())) {
            event.setCancelled(true);
        }
    }
    
    private static BlockSpread blockSpread;
    
    /**
     * Calls the registering of the BlockSpread {@link Listener}.
     */
    public static void register() {
        blockSpread = new BlockSpread();
        RegisterGameEvents.reg(blockSpread);
    }
    
    /**
     * Calls the unregistering of the BlockSpread {@link Listener}.
     */
    public static void unregister() {
        RegisterGameEvents.unreg(blockSpread);
        blockSpread = null;
    }
    
}