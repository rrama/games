package com.derplin.games.events.defaults;

import com.derplin.games.Games;
import com.derplin.games.events.RegisterGameEvents;
import java.io.File;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

public class WorldLoad implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onWorldLoad(WorldLoadEvent event) {
        if (!event.getWorld().getName().equals("world")) return;
        try {
            Games.mapMeta = YamlConfiguration.loadConfiguration(new File(event.getWorld().getWorldFolder(), "MapMeta.yml"));
        } catch (Exception ex) {
            Games.mapMeta = null;
        }
        Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam("Refs").setPrefix(ChatColor.GOLD.toString());
        RegisterGameEvents.unreg(this);
        Games.game.onMainWorldLoad();
    }
    
}