package com.derplin.games.events;

import com.derplin.games.Games;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class XPKill implements Listener {
    
    public static int xp = 1;
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player killer = event.getEntity().getKiller();
        if (killer == null) return;
        Games.awardXP(killer, xp);
    }
    
    private static XPKill xpKill;
    
    /**
     * Calls the registering of the XPKill {@link Listener}.
     */
    public static void register() {
        xpKill = new XPKill();
        RegisterGameEvents.reg(xpKill);
    }
    
    /**
     * Calls the unregistering of the XPKill {@link Listener}.
     */
    public static void unregister() {
        RegisterGameEvents.unreg(xpKill);
        xpKill = null;
    }
    
}