package com.derplin.games.events.region;

import com.derplin.games.commands.Ref;
import com.derplin.games.events.RegisterGameEvents;
import com.derplin.games.regions.PlayerIsInEvented;
import com.derplin.games.regions.Region2D;
import com.derplin.games.regions.RegionUtil;
import java.util.ArrayList;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerIsIn implements Listener {
    
    private static final ArrayList<PlayerIsInEvented> evented = new ArrayList<>(); //TODO - And extends Region2D?
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (Ref.isRef(event.getPlayer())) return;
        if (event.getTo().toVector().equals(event.getFrom().toVector())) return;
        int x = event.getTo().getBlockX();
        int y = event.getTo().getBlockY();
        int z = event.getTo().getBlockZ();
        for (PlayerIsInEvented e : evented) {
            if (RegionUtil.fullyContains((Region2D) e, x, y, z)) {
                e.onPlayerIsIn(event);
            }
        }
    }
    
    public static ArrayList<PlayerIsInEvented> getEvented() {
        return evented;
    }
    
    public static <T extends Region2D & PlayerIsInEvented> void addEvented(T e) {
        evented.add(e);
        if (playerIsInEvent == null) {
            register();
        }
    }
    
    public static void removeEvented(PlayerIsInEvented e) {
        evented.remove(e);
        if (evented.isEmpty()) {
            unregister();
        }
    }
    
    public static void clearEvented() {
        evented.clear();
        unregister();
    }
    
    private static PlayerIsIn playerIsInEvent = null;
    
    /**
     * Calls the registering of the PlayerEnterEvent {@link Listener}.
     */
    private static void register() {
        playerIsInEvent = new PlayerIsIn();
        RegisterGameEvents.reg(playerIsInEvent);
    }
    
    /**
     * Calls the unregistering of the PlayerEnterEvent {@link Listener}.
     */
    private static void unregister() {
        RegisterGameEvents.unreg(playerIsInEvent);
        playerIsInEvent = null;
    }
    
}