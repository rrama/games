package com.derplin.games.events;

import com.derplin.games.events.defaults.WorldLoad;
import com.derplin.games.Games;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

/**
 * Calls for registering {@link Event}s are in this class.
 */
public class RegisterGameEvents {
    
    /**
     * Registers all the always-running {@link Event}s.
     */
    public static void register() {
        reg(new WorldLoad());
    }
    
    /**
     * Registers the {@link Event} {@link Listener}.
     * @param ll the {@link Listener} to register.
     */
    public static void reg(Listener ll) {
        Bukkit.getPluginManager().registerEvents(ll, Games.plugin);
    }
    
    /**
     * Unregisters the {@link Event} {@link Listener}.
     * @param ll the {@link Listener} to unregister.
     */
    public static void unreg(Listener ll) {
        HandlerList.unregisterAll(ll);
    }
    
}