package com.derplin.games.events;

import com.derplin.games.Games;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class Thrower implements Listener {
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (event.isCancelled()) return;
        event.getItemDrop().setMetadata("Thrower", new FixedMetadataValue(Games.plugin, event.getPlayer().getName()));
    }
    
    private static Thrower thrower;
    
    /**
     * Calls the registering of the Thrower {@link Listener}.
     */
    public static void register() {
        thrower = new Thrower();
        RegisterGameEvents.reg(thrower);
    }
    
    /**
     * Calls the unregistering of the Thrower {@link Listener}.
     */
    public static void unregister() {
        RegisterGameEvents.unreg(thrower);
        thrower = null;
    }
    
}