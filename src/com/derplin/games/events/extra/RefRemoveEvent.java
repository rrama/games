package com.derplin.games.events.extra;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RefRemoveEvent extends PlayerEvent implements Cancellable {
    
    private static final HandlerList handlers = new HandlerList();
    private boolean cancel = false;
    private String disallowMsg = ChatColor.YELLOW + "You can not be removed from being a ref at this time";
    
    public RefRemoveEvent(Player p) {
        super(p);
    }
    
    public String getDisallowMsg() {
        return disallowMsg;
    }
    
    public void setDisallowMsg(String msg) {
        disallowMsg = msg;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    @Override
    public boolean isCancelled() {
        return cancel;
    }
    
    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }
    
}