package com.derplin.games;

import com.derplin.games.gameutil.DatabaseBase;
import com.derplin.games.achievements.Achievement;
import com.derplin.games.achievements.RegisterAchievements;
import com.derplin.games.commands.LevelCmd;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * The database used by the {@link Games} plugin.
 */
public class GamesDatabase extends DatabaseBase {
    
    private static final String A_TABLE = "player_awards";
    private static final String L_TABLE = "player_levels";

    /**
     * The constructor for the database.
     */
    public GamesDatabase() {
        super(FolderSetup.database);
        executeStatement("CREATE TABLE IF NOT EXISTS " + A_TABLE + " (name TEXT UNIQUE PRIMARY KEY NOT NULL)");
        executeStatement("CREATE TABLE IF NOT EXISTS " + L_TABLE + " (name TEXT UNIQUE PRIMARY KEY NOT NULL, "
                         + "level INTEGER NOT NULL DEFAULT 0,"
                         + "xp INTEGER NOT NULL DEFAULT 0)");
    }

    /**
     * Checks if if <code>playerName</code> has the Achievement <code>achievement</code> in the database.
     *
     * @param playerName  the name of the {@link Player} to check if the have the {@link Achievement}.
     * @param achievement the {@link Achievement}'s SQL name to check if the {@link Player} has.
     * @return {@code true} if <code>playerName</code> has the {@link Achievement} <code>achievement</code>.
     * @throws Exception on any SQLException or if the {@link Player}'s record is not found (attempts to create as well).
     */
    public boolean hasAchievement(String playerName, String achievement) throws Exception {
        ResultSet rs = getPlayerRecord(playerName, A_TABLE);
        if (rs == null) throw new Exception("SQL error");
        try {
            return rs.getBoolean(achievement);
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
            throw new Exception("SQL error");
        }
    }

    /**
     * Sets <code>playerName</code> to having the Achievement <code>achievement</code> in the database.
     *
     * @param playerName  the {@link Player}'s name who is being awarded the {@link Achievement}.
     * @param achievement the {@link Achievement}'s SQL name that is being awarded.
     * @return {@code true} if there are no problems awarding the {@link Achievement}.
     */
    public boolean awardAchievement(String playerName, String achievement) {
        return updateRecord(playerName, A_TABLE, achievement, "1");
    }

    /**
     * Used by the /Achievements command.
     * If database is down it will return "Achievements database is currently down."
     *
     * @param playerName the name of the {@link Player} to get the {@link Achievement}s of.
     * @return A list of {@link String}s with {@link Plugin} names then their {@link Achievement}s.
     *         Red means they haven't got the achievement, green means they have it,
     *         Black means there was an error checking.
     */
    public ArrayList<String> getAchievements(String playerName) {
        ResultSet rs = getPlayerRecord(playerName, A_TABLE);
        ArrayList<String> done = new ArrayList<>();
        if (rs == null) {
            done.add(ChatColor.YELLOW + "Achievements database is currently down.");
            return done;
        }
        HashMap<String, StringBuilder> achievementBuilder = new HashMap<>();
        for (Achievement achievement : RegisterAchievements.getAchievements()) {
            String plugin = achievement.getOwningPlugin();
            if (!achievementBuilder.containsKey(plugin)) {
                achievementBuilder.put(plugin, new StringBuilder(ChatColor.YELLOW + plugin).append(": "));
            }
            StringBuilder builder = achievementBuilder.get(plugin);
            try {
                builder.append(rs.getBoolean(achievement.getSQLName()) ? ChatColor.GREEN : ChatColor.RED);
            } catch (SQLException ex) {
                Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
                builder.append(ChatColor.BLACK);
            }
            builder.append(achievement.getName());
        }
        for (StringBuilder builder : achievementBuilder.values()) {
            done.add(builder.toString());
        }
        return done;
    }

    /**
     * Makes a column in the table for the {@link Achievement} if it doesn't already exist.
     *
     * @param achievement the SQL name of the {@link Achievement}.
     * @return {@code true} if there is now a column by that name.
     */
    public boolean makeAchievementColumn(String achievement) {
        ResultSet rs = executeQuery("PRAGMA table_info(" + A_TABLE + ")");
        if (rs == null) return false;
        try {
            do {
                if (rs.getString("name").equals(achievement)) return true;
            } while (rs.next());
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, null, ex);
            return false;
        }
        return executeStatement("ALTER TABLE " + A_TABLE + " ADD COLUMN " + achievement + " INTEGER NOT NULL DEFAULT 0");
    }
    
    /**
     * Gets the level of the {@link Player} specified.
     * 
     * @param playerName the name of the {@link Player}.
     * @return the level of the {@link Player}.
     * @throws Exception on any SQLException or if the {@link Player}'s record is not found (attempts to create as well).
     */
    public int getPlayerLevel(String playerName) throws Exception {
        ResultSet rs = getPlayerRecord(playerName, L_TABLE);
        if (rs == null) throw new Exception("SQL error");
        try {
            return rs.getInt("level");
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
            throw new Exception("SQL error");
        }
    }
    
    /**
     * Gets the XP to the next level of the {@link Player} specified.
     * 
     * @param playerName the name of the {@link Player}.
     * @return the XP to the next level of the {@link Player}.
     * @throws Exception on any SQLException or if the {@link Player}'s record is not found (attempts to create as well).
     */
    public int getPlayerXP(String playerName) throws Exception {
        ResultSet rs = getPlayerRecord(playerName, L_TABLE);
        if (rs == null) throw new Exception("SQL error");
        try {
            return rs.getInt("xp");
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
            throw new Exception("SQL error");
        }
    }
    
    /**
     * Gets the level and the XP to the next level of the {@link Player} specified.
     * 
     * @param playerName the name of the {@link Player}.
     * @return a {@link List} containing the level and the XP to the next level of the {@link Player}.
     *         Element 0 is the level and element 1 is the XP to the next level.
     * @throws Exception on any SQLException or if the {@link Player}'s record is not found (attempts to create as well).
     */
    public List<Integer> getPlayerLevelAndXP(String playerName) throws Exception {
        ResultSet rs = getPlayerRecord(playerName, L_TABLE);
        if (rs == null) throw new Exception("SQL error");
        try {
            return Arrays.asList(rs.getInt("level"), rs.getInt("xp"));
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
            throw new Exception("SQL error");
        }
    }
    
    /**
     * Adds experience to the {@link Player} and levels them up if needed.
     * 
     * @param playerName the {@link Player}'s name who is being awarded the experience.
     * @param xp the amount of experience to add. Not negative.
     * @return {@code true} if XP was added with no errors.
     */
    public boolean addPlayerXP(String playerName, int xp) {
        if (xp < 1) return true;
        try {
            List<Integer> levelAndXP = getPlayerLevelAndXP(playerName);
            int level = levelAndXP.get(0);
            xp += levelAndXP.get(1);
            int levelUpXP = LevelCmd.xpToNextLevel(level);
            while (xp >= levelUpXP) {
                ++level;
                xp -= levelUpXP;
                levelUpXP = LevelCmd.xpToNextLevel(level);
            }
            if (xp != levelAndXP.get(1)) {
                if (!updateRecord(playerName, L_TABLE, "xp", String.valueOf(xp))) return false;
            }
            if (level != levelAndXP.get(0)) {
                if (!updateRecord(playerName, L_TABLE, "level", String.valueOf(level))) return false;
                Player p = Bukkit.getPlayer(playerName);
                if (p != null) {
                    p.sendMessage(ChatColor.GOLD + "You leveled up to level " + ChatColor.RED + level + ChatColor.GOLD + "!");
                    p.playSound(p.getLocation(), Sound.LEVEL_UP, 3, 1);
                }
            }
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    
}