package com.derplin.games;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

/**
 * Useful methods that are (will be) used throughout the plugin or by dependencies.
 */
public class Util {
    
    /**
     * Checks to see if the {@link String} provided matches the format of an {@link Integer}.
     * @param s the {@link String} to check if it is an {@link Integer}.
     * @return {@code true} if <code>s</code> matches the format of an {@link Integer}.
     */
    public static boolean isInteger(String s) {
        return s.matches("-?\\d+(.\\d+)?");
    }
    
    /**
     * Gets the {@link OfflinePlayer} with a name matching <code>name</code>.
     * If that fails it then tries to match against an online {@link Player}.
     * If that fails it returns {@code null}.
     *
     * @param name the name to match against.
     * @return an {@link OfflinePlayer} or {@code null}.
     */
    public static OfflinePlayer getOfflinePlayer(String name) {
        OfflinePlayer op = Bukkit.getOfflinePlayer(name);
        if (op.hasPlayedBefore()) {
            return op;
        }
        return Bukkit.getPlayer(name);
    }

    /**
     * Copies a folder and all its content to the new folder.
     * New folder doesn't have to be created yet.
     *
     * @param srcDir  the folder to copy.
     * @param destDir the new location of the folder.
     * @throws IOException
     */
    public static void copyDirectory(File srcDir, File destDir) throws IOException {
        destDir.mkdirs();
        for (File srcFile : srcDir.listFiles()) {
            File dstFile = new File(destDir, srcFile.getName());
            if (srcFile.isDirectory()) {
                copyDirectory(srcFile, dstFile);
            } else {
                copyFile(srcFile, dstFile);
            }
        }
    }

    /**
     * Copies a {@link File} from one location to another, copying its content.
     *
     * @param srcFile  the {@link File} to copy.
     * @param destFile the new location of the {@link File}.
     * @throws IOException
     */
    public static void copyFile(File srcFile, File destFile) throws IOException {
        if (destFile.exists()) {
            destFile.delete();
        }

        try (FileInputStream fis = new FileInputStream(srcFile);
             FileOutputStream fos = new FileOutputStream(destFile);
             FileChannel input = fis.getChannel();
             FileChannel output = fos.getChannel()) {
            long size = input.size();
            long pos = 0L;
            long count;
            while (pos < size) {
                count = size - pos > 31457280L ? 31457280L : size - pos;
                pos += output.transferFrom(input, pos, count);
            }
        }

        if (srcFile.length() != destFile.length()) { //Safety check.
            throw new IOException("Failed to copy full contents from '" + srcFile + "' to '" + destFile + "'");
        }
    }
    
    /**
     * Deletes a folder and all its sub-folders and {@link File}s.
     * @param file the folder or {@link File} to clear and delete.
     * @return {@code true} if the <code>file</code> was deleted.
     */
    public static boolean deleteDirectory(File file) {
        if (file.isDirectory()) {
            boolean worked = true;
            for (File inner : file.listFiles()) {
                if (!deleteDirectory(inner)) {
                    worked = false;
                }
            }
            return file.delete() && worked;
        } else {
            return file.delete();
        }
    }
    
    /**
     * Whether or not the {@link Player} has free enough room
     * to also hold the {@link ItemStack} specified.
     * @param p   the {@link Player} to check if they have
     *            enough {@link Inventory} space.
     * @param iss the {@link ItemStack} to check if it would fits.
     * @return {@code true} if the {@link Player} has enough free room
     *         in their {@link PlayerInventory} to hold <code>iss</code>.
     */
    public static boolean hasSpaceFor(Player p, ItemStack iss) {
        Inventory inv = p.getInventory();
        int freeSpace = 0;
        for (int i = 0; i < 36; i++) {
            ItemStack item = inv.getItem(i);
            if (item == null) {
                freeSpace += iss.getMaxStackSize();
            } else if (item.isSimilar(iss)) {
                freeSpace += iss.getMaxStackSize() - item.getAmount();
            }
        }
        return freeSpace >= iss.getAmount();
    }
    
    public static boolean matchesItemStack(ItemStack iss, Material mat, String name, String... lore) {
        if (iss.getType() != mat) return false;
        return matchesItemStack(iss, name, lore);
    }
    
    public static boolean matchesItemStack(ItemStack iss, String name, String... lore) {
        try {
            if (!iss.hasItemMeta()) return name == null && lore == null;
            ItemMeta im = iss.getItemMeta();
            
            if (name == null) {
                if (!im.getDisplayName().equalsIgnoreCase(iss.getType().name())) return false;
            } else {
                if (!im.getDisplayName().matches(name)) return false;
            }
            
            if (lore == null || lore.length == 0) {
                return !im.hasLore();
            } else {
                List<String> imLore = im.getLore();
                if (lore.length != imLore.size()) return false;
                for (int i = 0; i < lore.length; i++) {
                    if (!lore[i].equals(imLore.get(i))) return false;
                }
            }
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    
    public static Vector stringToVector(String str) {
        try {
            String[] strs = str.split(", ");
            return new Vector(Double.valueOf(strs[0]), Double.valueOf(strs[1]), Double.valueOf(strs[2]));
        } catch (Exception ex) {
            return null;
        }
    }
    
}