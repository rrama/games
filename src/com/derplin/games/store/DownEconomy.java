package com.derplin.games.store;

import com.derplin.games.store.EconomyException.Action;
import com.derplin.games.store.EconomyException.FailReason;

/**
 * The default {@link Economy} used by {@link StoreCmd}.
 * All methods throw {@link EconomyException} with a {@link FailReason} of {@code DOWN}.
 */
public class DownEconomy extends Economy {

    @Override
    public int getBalance(String pn) throws EconomyException {
        throw new EconomyException(Action.GET, FailReason.DOWN);
    }

    @Override
    public void giveMoney(String pn, int amount) throws EconomyException {
        throw new EconomyException(Action.GIVE, FailReason.DOWN);
    }

    @Override
    public void takeMoney(String pn, int amount) throws EconomyException {
        throw new EconomyException(Action.TAKE, FailReason.DOWN);
    }
    
}