package com.derplin.games.store;

import com.derplin.games.Util;
import com.derplin.games.achievements.StoreAchieve;
import com.derplin.games.commands.SenderUtil;
import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * The /Store and /Buy {@link CommandExecutor}.
 */
public class StoreCmd implements CommandExecutor {
    
    public static final ArrayList<StoreItem> items = new ArrayList<>();
    public static Economy eco = new DownEconomy();
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (SenderUtil.isntPlayerMsg(sender)) return true;
        Player p = (Player) sender;
        new StoreAchieve().awardAchievement(p.getName());
        switch (commandLabel.toLowerCase()) {
            case "store": case "shop":
                browse(p);
                return true;
            case "buy":
                if (args.length != 1) return false;
                return buy(p, args[0]);
            default:
                return false;
        }
    }
    
    private void browse(Player p) {
        p.sendMessage(ChatColor.GREEN + "Store:"); //TODO - Nicen.
        boolean nothing = true;
        for (int i = 1; i <= items.size(); i++) {
            StoreItem item = items.get(i-1);
            if (item.isVisibleTo(p)) {
                nothing = false;
                p.sendMessage(ChatColor.GREEN.toString() + i + ". " + item.getName() + " - " + item.getPrice() + " cookies."); //TODO - Nicen.
            }
        }
        if (nothing) {
            p.sendMessage(ChatColor.YELLOW + "Nothing is avalible to buy right now.");
        } else {
            p.sendMessage(ChatColor.GREEN + "Use /Buy # to buy stuff."); //TODO - Nicen.
        }
    }
    
    private boolean buy(Player p, String arg0) {
        if (!Util.isInteger(arg0)) return false;
        int num = Integer.parseInt(arg0);

        if (num < 1 || num > items.size()) {
            p.sendMessage(ChatColor.YELLOW + "Enter a valid store item.");
            return false;
        }

        StoreItem item = items.get(num - 1);

        if (!item.isVisibleTo(p)) {
            p.sendMessage(ChatColor.YELLOW + "Enter a valid store item.");
            return false;
        }
        if (!item.canGiveTo(p)) return false; //The method should message the player if they can't recieve it.

        try {
            eco.takeMoney(p, item.getPrice());
        } catch (EconomyException ex) {
            switch (ex.getFailReason()) {
                case DOWN:
                    p.sendMessage(ChatColor.YELLOW + "Economy system failed, it might be down, please try again.");
                    return false;
                case INSUFFICIENT_FUNDS:
                    p.sendMessage(ChatColor.YELLOW + "You can not afford this item.");
                    return false;
                case UNKNOWN:
                    p.sendMessage(ChatColor.YELLOW + "Economy system failed, with an unknown reason, please try again.");
                    return false;
            }
        }

        if (!item.giveTo(p)) { //Final check incase something goes wrong.
            p.sendMessage(ChatColor.YELLOW + "Woops something went wrong giving you the item. Refunding now.");
            try {
                eco.giveMoney(p, item.getPrice());
            } catch (EconomyException ex) {
                p.sendMessage(ChatColor.YELLOW + "Unable to refund :/ Reason '" + ex.getFailReason().name().toLowerCase() + "'.");
            }
        }
        return true;
    }
    
}