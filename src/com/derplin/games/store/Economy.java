package com.derplin.games.store;

import org.bukkit.entity.Player;

/**
 * An abstract class that can be overridden to allow the shop
 * command to use economical features from different sources.
 */
public abstract class Economy {
    
    /**
     * Gets the balance of a {@link Player}.
     * @param p the {@link Player} to check the balance of.
     * @return the balance of {@link Player} <code>p</code>.
     * @throws EconomyException if the economy is down or an error occurs.
     */
    public int getBalance(Player p) throws EconomyException {
        return getBalance(p.getName());
    }
    
    /**
     * Gets the balance of a {@link Player}.
     * @param pn the name of the {@link Player}.
     * @return the balance of {@link Player} with name <code>pn</code>.
     * @throws EconomyException if the economy is down or an error occurs.
     */
    public abstract int getBalance(String pn) throws EconomyException;
    
    /**
     * Adds money to the balance of a {@link Player}.
     * @param p the {@link Player} to give money to.
     * @param amount the amount of money to give <code>p</code>.
     * @throws EconomyException if the economy is down or an error occurs.
     */
    public void giveMoney(Player p, int amount) throws EconomyException {
        giveMoney(p.getName(), amount);
    }
    
    /**
     * Adds money to the balance of a {@link Player}.
     * @param pn the name of the {@link Player} to give money to.
     * @param amount the amount of money to give the {@link Player} with name <code>pn</code>.
     * @throws EconomyException if the economy is down or an error occurs.
     */
    public abstract void giveMoney(String pn, int amount) throws EconomyException;
    
    /**
     * Takes money from the balance of a {@link Player}.
     * {@link Player}s are unable to go into negative funds.
     * @param p the {@link Player} to take money from.
     * @param amount the amount of money to take from <code>p</code>.
     * @throws EconomyException if the {@link Player} has insufficient funds,
     *                          the economy is down or an error occurs.
     */
    public void takeMoney(Player p, int amount) throws EconomyException {
        takeMoney(p.getName(), amount);
    }
    
    /**
     * Takes money from the balance of a {@link Player}.
     * {@link Player}s are unable to go into negative funds.
     * @param pn the name of the {@link Player} to take money from.
     * @param amount the amount of money to take from the {@link Player} with name <code>pn</code>.
     * @throws EconomyException if the {@link Player} has insufficient funds,
     *                          the economy is down or an error occurs.
     */
    public abstract void takeMoney(String pn, int amount) throws EconomyException;
    
}