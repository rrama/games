package com.derplin.games.store;

import com.derplin.games.Util;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Represents a {@link StoreItem} that gives the {@link Player} an {@link ItemStack}.
 */
public class StoreItemStack extends StoreItem {
    
    public ItemStack iss;
    
    /**
     * The default constructor of a {@link StoreItemStack}.
     * @param name  the displayed name of the {@link StoreItemStack}.
     * @param price the price needed to buy 1.
     * @param iss   the {@link ItemStack} the {@link Player} will receive.
     */
    public StoreItemStack(String name, int price, ItemStack iss) {
        super(name, price);
        this.iss = iss;
    }
    
    @Override
    public boolean canGiveTo(Player p) {
        if (Util.hasSpaceFor(p, iss)) {
            return true;
        }
        p.sendMessage(ChatColor.YELLOW + "You do not have enough space in your inventory.");
        return false;
    }
    
    @Override
    public boolean giveTo(Player p) {
        return p.getInventory().addItem(iss).isEmpty();
    }
    
}