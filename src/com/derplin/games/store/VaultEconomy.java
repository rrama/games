package com.derplin.games.store;

import com.derplin.games.dependencies.VaultDanger;
import com.derplin.games.store.EconomyException.Action;
import com.derplin.games.store.EconomyException.FailReason;

/**
 * An {@link Economy} that uses Vault to manage the balance.
 */
public class VaultEconomy extends Economy {

    /**
     * Checks if there is an economy provider registered with Vault.
     * @return {@code true} if there is an economy provider registered.
     */
    public boolean canHookEconomy() {
        try {
            return VaultDanger.canHookEconomy();
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {
            return false;
        }
    }
    
    @Override
    public int getBalance(String pn) throws EconomyException {
        try {
            return (int)VaultDanger.getBalance(pn);
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {
            throw new EconomyException(Action.GET, FailReason.DOWN);
        }
    }
    
    @Override
    public void giveMoney(String pn, int amount) throws EconomyException {
        try {
            if (!VaultDanger.giveMoney(pn, amount)) {
                throw new EconomyException(Action.GIVE, FailReason.UNKNOWN);
            }
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {
            throw new EconomyException(Action.GIVE, FailReason.DOWN);
        }
    }
    
    @Override
    public void takeMoney(String pn, int amount) throws EconomyException {
        try {
            if (getBalance(pn) < amount) {
                throw new EconomyException(Action.TAKE, FailReason.INSUFFICIENT_FUNDS);
            }
            if (!VaultDanger.takeMoney(pn, amount)) {
                throw new EconomyException(Action.TAKE, FailReason.UNKNOWN);
            }
        } catch (ClassNotFoundException | NoClassDefFoundError ex) {
            throw new EconomyException(Action.TAKE, FailReason.DOWN);
        }
    }
    
}