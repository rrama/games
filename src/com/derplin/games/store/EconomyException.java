package com.derplin.games.store;

/**
 * The {@link Exception} that can be thrown by {@link Economy} methods.
 */
public class EconomyException extends Exception {
    
    private final Action action;
    private final FailReason failReason;
    
    /**
     * The default constructor of an {@link EconomyException}.
     * @param action     the action being performed that threw the {@link EconomyException}.
     * @param failReason the generic reason why the action failed.
     */
    public EconomyException(Action action, FailReason failReason) {
        this.action = action;
        this.failReason = failReason;
    }
    
    /**
     * Gets the action that was being performed that caused {@link EconomyException} to be thrown.
     * @return the action that threw the {@link EconomyException}.
     */
    public Action getAction() {
        return action;
    }
    
    /**
     * Gets the generic reason why the action failed.
     * @return the generic reason why the action failed.
     */
    public FailReason getFailReason() {
        return failReason;
    }
    
    /**
     * An action that can be performed by a class inheriting {@link Economy}.
     */
    public enum Action {

        /**
         * Getting the balance of the {@link Player}.
         */
        GET,

        /**
         * Giving money to the {@link Player}.
         */
        GIVE,

        /**
         * Taking money from the {@link Player}.
         */
        TAKE;
    }
    
    /**
     * A generic reason why an {@link Economy} {@link Action} could fail.
     */
    public enum FailReason {

        /**
         * The {@link Economy} is down.
         */
        DOWN,

        /**
         * The {@link Player} does not have enough money for the take to occur.
         */
        INSUFFICIENT_FUNDS,

        /**
         * An unknown error occurred.
         */
        UNKNOWN;
    }
    
}