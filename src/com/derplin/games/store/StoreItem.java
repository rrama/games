package com.derplin.games.store;

import org.bukkit.entity.Player;

/**
 * An item that can be bought from the /Store.
 */
public abstract class StoreItem {
    
    final String name;
    final int price;
    
    /**
     * The default constructor of a {@link StoreItem}.
     * @param name  the displayed name of the {@link StoreItem}.
     * @param price the price needed to buy 1.
     */
    public StoreItem(String name, int price) {
        this.name = name;
        this.price = price;
    }
    
    /**
     * Gets the user-friendly name of the {@link StoreItem}.
     * @return the name of the {@link StoreItem}.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Gets the price needed to buy this {@link StoreItem}.
     * @return the price needed to buy 1.
     */
    public int getPrice() {
        return price;
    }
    
    /**
     * Whether or not the {@link Player} will see the item in the /Store command.
     * @param p the {@link Player} to test against.
     * @return {@code true} if the {@link Player} is able to see the item in the /Store command.
     */
    public boolean isVisibleTo(Player p) {
        return true;
    }
    
    /**
     * Whether or not the {@link Player} is able to receive
     * this {@link StoreItem} at the current time.
     * @param p the {@link Player} to test.
     * @return {@code true} if the {@link Player} is able to receive.
     */
    public boolean canGiveTo(Player p) {
        return true;
    }
    
    /**
     * The method used to give a {@link Player} this {@link StoreItem}.
     * @param p the {@link Player} to give the item to.
     * @return {@code true} if <code>p</code> received the item correctly.
     */
    public abstract boolean giveTo(Player p);
    
}