package com.derplin.games.regions;

public class RegionUtil {
    
    public static boolean fullyContains(Region2D region, int x, int y, int z) {
        if (region instanceof Region3D) {
            return ((Region3D) region).contains(x, y, z);
        } else {
            return ((Region2D) region).contains(x, z);
        }
    }
    
}