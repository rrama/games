package com.derplin.games.regions;

import com.derplin.games.Games;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Region3D extends Region2D {
    
    private final int maxY, minY;
    
    public Region3D(Vector bMax, Vector bMin) {
        this(bMax.getBlockX(), bMax.getBlockY(), bMax.getBlockZ(),
             bMin.getBlockX(), bMin.getBlockY(), bMin.getBlockZ());
    }
    
    public Region3D(int maxX, int maxY, int maxZ, int minX, int minY, int minZ) {
        super(maxX, maxZ, minX, minZ);
        this.maxY = maxY;
        this.minY = minY;
    }
    
    public int getMaxY() {
        return maxY;
    }
    
    public int getMinY() {
        return minY;
    }
    
    public boolean contains(int x, int y, int z) {
        return super.contains(x, z) && getMinY() <= y && y <= getMaxY();
    }
    
    @Override
    public Location getCentre() {
        int midX = (int)(getMaxX() + getMinX())/2;
        int midY = (int)(getMaxY() + getMinY())/2;
        int midZ = (int)(getMaxZ() + getMinZ())/2;
        return new Location(Bukkit.getWorlds().get(0), midX, midY, midZ);
    }
    
    @Override
    public Location getRandomPoint() {
        Random random = new Random();
        int randomX = random.nextInt(getMaxX() - getMinX()) + getMinX();
        int randomY = random.nextInt(getMaxY() - getMinY()) + getMinY();
        int randomZ = random.nextInt(getMaxZ() - getMinZ()) + getMinZ();
        return new Location(Bukkit.getWorlds().get(0), randomX, randomY, randomZ);
    }
    
    @Override
    public String toString() {
        return "Region3D{MAX=(" + getMaxX() + ", " + getMaxY() + ", " + getMaxZ() + ") MIN=(" + getMinX() + ", " + getMinY() + ", " + getMinZ() + ")}";
    }
    
}