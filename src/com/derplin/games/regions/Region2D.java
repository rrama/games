package com.derplin.games.regions;

import com.derplin.games.Games;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Region2D {
    
    private final int maxX, maxZ, minX, minZ;
    
    public Region2D(int maxX, int maxZ, int minX, int minZ) {
        this.maxX = maxX;
        this.maxZ = maxZ;
        this.minX = minX;
        this.minZ = minZ;
    }
    
    public int getMaxX() {
        return maxX;
    }
    
    public int getMaxZ() {
        return maxZ;
    }
    
    public int getMinX() {
        return minX;
    }
    
    public int getMinZ() {
        return minZ;
    }
    
    public boolean contains(int x, int z) {
        return getMinX() <= x && x <= getMaxX() && getMinZ() <= z && z <= getMaxZ();
    }
    
    public Location getCentre() {
        int midX = (int)(getMaxX() + getMinX())/2;
        int midZ = (int)(getMaxZ() + getMinZ())/2;
        return Bukkit.getWorlds().get(0).getHighestBlockAt(midX, midZ).getLocation();
    }
    
    public Location getRandomPoint() {
        Random random = new Random();
        int randomX = random.nextInt(getMaxX() - getMinX()) + getMinX();
        int randomZ = random.nextInt(getMaxZ() - getMinZ()) + getMinZ();
        return Bukkit.getWorlds().get(0).getHighestBlockAt(randomX, randomZ).getLocation();
    }
    
    @Override
    public String toString() {
        return "Region2D{MAX=(" + getMaxX() + ", " + getMaxZ() + ") MIN=(" + getMinX() + ", " + getMinZ() + ")}";
    }
    
}