package com.derplin.games.regions;

import org.bukkit.event.player.PlayerMoveEvent;

public interface PlayerIsInEvented {
    
    public void onPlayerIsIn(PlayerMoveEvent event);
    
    public void onPlayerNotIn(PlayerMoveEvent event);
    
}