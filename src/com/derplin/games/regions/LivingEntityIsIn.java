package com.derplin.games.regions;

import com.derplin.games.Games;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitTask;

public class LivingEntityIsIn {
    
    private static final ArrayList<LivingEntityIsInEvented> evented = new ArrayList<>(); //TODO - And extends Region2D?
    
    public static <T extends Region2D & LivingEntityIsInEvented> void addEvented(T e) {
        evented.add(e);
        if (task == null) {
            register();
        }
    }
    
    public static ArrayList<LivingEntityIsInEvented> getEvented() {
        return evented;
    }
    
    public static void removeEvented(LivingEntityIsInEvented e) {
        evented.remove(e);
        if (evented.isEmpty()) {
            unregister();
        }
    }
    
    public static void clearEvented() {
        evented.clear();
        unregister();
    }
    
    private static BukkitTask task = null;
    
    /**
     * Calls the registering of the LivingEntityEnter {@link BukkitTask}.
     */
    private static void register() {
        task = Bukkit.getScheduler().runTaskTimer(Games.plugin, new Runnable() {

            @Override
            public void run() {
                for (LivingEntity le : Bukkit.getWorlds().get(0).getLivingEntities()) {
                    int x = le.getLocation().getBlockX();
                    int y = le.getLocation().getBlockY();
                    int z = le.getLocation().getBlockZ();
                    for (LivingEntityIsInEvented leiie : evented) {
                        if (leiie.acceptsEntityType(le.getType()) && RegionUtil.fullyContains((Region2D) leiie, x, y, z)) {
                            leiie.onLivingEntityIsIn(le);
                        }
                    }
                }
            }
            
        }, 40, 40);
    }
    
    /**
     * Calls the unregistering of the LivingEntityEnter {@link BukkitTask}.
     */
    private static void unregister() {
        task.cancel();
        task = null;
    }
    
}