package com.derplin.games.regions;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

public interface LivingEntityIsInEvented {
    
    public void onLivingEntityIsIn(LivingEntity entity);
    
    public boolean acceptsEntityType(EntityType et);
    
}