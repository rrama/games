package com.derplin.games.commands;

import com.derplin.games.Util;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Useful methods for that will be called by {@link CommandExecutor}s.
 */
public class SenderUtil {
    
    /**
     * Checks if the <code>sender</code> is a Player, if they aren't it messages them.
     * The message is "You must be a Player to use this command."
     * @param sender The CommandSender to check and message.
     * @return {@code true} if they aren't a Player.
     */
    public static boolean isntPlayerMsg(CommandSender sender) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "You must be a Player to use this command.");
            return true;
        }
        return false;
    }
    
    /**
     * Checks if the <code>sender</code> is a BlockCommandSender, if they aren't it messages them.
     * The message is "This is a command block command."
     * @param sender The CommandSender to check and message.
     * @return {@code true} if they aren't a BlockCommandSender.
     */
    public static boolean isntCmdBlockMsg(CommandSender sender) {
        if (!(sender instanceof BlockCommandSender)) {
            sender.sendMessage(ChatColor.RED + "This is a command block command.");
            return true;
        }
        return false;
    }
    
    /**
     * Gets the {@link OfflinePlayer} with a name matching <code>offlinePlayerName</code>.
     * If that fails it then tries to match against an online {@link Player}.
     * If that fails it returns {@code null}
     * and messages <code>sender</code> saying "Can not find the online/offline player specified.".
     * 
     * @param sender the {@link CommandSender} to message if there is no one by that name.
     * @param offlinePlayerName the name to match against.
     * @return an {@link OfflinePlayer} or {@code null}.
     * @see Util#getOfflinePlayer(String)
     */
    public static OfflinePlayer getOfflinePlayer(CommandSender sender, String offlinePlayerName) {
        OfflinePlayer op = Util.getOfflinePlayer(offlinePlayerName);
        if (op == null) {
            sender.sendMessage(ChatColor.YELLOW + "Can not find the online/offline player specified.");
        }
        return op;
    }
    
    /**
     * Gets the {@link OfflinePlayer} with a name matching <code>offlinePlayerName</code> and returns their name.
     * If that fails it then tries to match against an online {@link Player} and return their name.
     * If that fails it returns {@code null}
     * and messages <code>sender</code> saying "Can not find the online/offline player specified.".
     * 
     * @param sender the {@link CommandSender} to message if there is no one by that name.
     * @param offlinePlayerName the name to match against.
     * @return the matched name of <code>offlinePlayerName</code> or {@code null}.
     * @see #getOfflinePlayer(CommandSender, String)
     */
    public static String getOfflinePlayerName(CommandSender sender, String offlinePlayerName) {
        OfflinePlayer op = getOfflinePlayer(sender, offlinePlayerName);
        return op == null ? null : op.getName();
    }
    
}