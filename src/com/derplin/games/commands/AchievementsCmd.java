package com.derplin.games.commands;

import com.derplin.games.Games;
import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * The /Achievement {@link CommandExecutor}.
 */
public class AchievementsCmd implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        String whom;
        if (args.length == 0) {
            if (SenderUtil.isntPlayerMsg(sender)) return false;
            whom = sender.getName();
        } else {
            whom = SenderUtil.getOfflinePlayerName(sender, args[0]);
            if (whom == null) return false;
        }

        ArrayList<String> list = Games.getGamesDatabase().getAchievements(whom);
        sender.sendMessage(ChatColor.YELLOW + "Achievements:"); //TODO - Make fancy.
        for (String msg : list) {
            sender.sendMessage(msg);
        }
        return true;
    }
}