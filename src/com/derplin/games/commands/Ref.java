package com.derplin.games.commands;

import com.derplin.games.events.extra.RefAddEvent;
import com.derplin.games.events.extra.RefRemoveEvent;
import java.util.ArrayList;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

public class Ref implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (SenderUtil.isntPlayerMsg(sender)) return true;
        Player p = (Player) sender;
        if (args.length == 0) {
            if (isRef(p)) {
                removeRef(p);
            } else {
                addRef(p);
            }
            return true;
        } else if (args.length == 1) {
            switch (args[0].toLowerCase()) {
                case "off":
                    removeRef(p);
                    return true;
                case "on":
                    addRef(p);
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }
    
    public static void addRef(Player p) {
        if (isRef(p)) {
            p.sendMessage(ChatColor.YELLOW + "You are already a ref.");
            return;
        }
        RefAddEvent event = new RefAddEvent(p);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            p.sendMessage(event.getDisallowMsg());
        } else {
            p.sendMessage(ChatColor.YELLOW + "You are now reffing.");
            getRefTeam().addPlayer(p);
        }
    }
    
    public static void removeRef(Player p) {
        if (!isRef(p)) {
            p.sendMessage(ChatColor.YELLOW + "You are already not a ref.");
            return;
        }
        RefRemoveEvent event = new RefRemoveEvent(p);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) {
            p.sendMessage(event.getDisallowMsg());
        } else {
            p.sendMessage(ChatColor.YELLOW + "You are no longer reffing.");
            getRefTeam().removePlayer(p);
        }
    }
    
    public static Set<OfflinePlayer> getRefs() {
        return getRefTeam().getPlayers();
    }
    
    public static ArrayList<Player> getOnlineRefs() {
        ArrayList<Player> refs = new ArrayList<>();
        for (OfflinePlayer op : getRefs()) {
            if (op.isOnline()) {
                refs.add(op.getPlayer());
            }
        }
        return refs;
    }
    
    public static ArrayList<String> getRefNames() {
        ArrayList<String> refs = new ArrayList<>();
        for (OfflinePlayer op : getRefs()) {
            refs.add(op.getName());
        }
        return refs;
    }
    
    public static ArrayList<String> getOnlineRefNames() {
        ArrayList<String> refs = new ArrayList<>();
        for (OfflinePlayer op : getRefs()) {
            if (op.isOnline()) {
                refs.add(op.getName());
            }
        }
        return refs;
    }
    
    public static boolean isRef(Player p) {
        return getRefTeam().hasPlayer(p);
    }
    
    public static boolean isRef(String pn) {
        return getRefTeam().hasPlayer(Bukkit.getOfflinePlayer(pn));
    }
    
    public static Team getRefTeam() {
        return Bukkit.getScoreboardManager().getMainScoreboard().getTeam("Refs");
    }
    
}