package com.derplin.games.commands;

import com.derplin.games.store.StoreCmd;
import com.derplin.games.Games;
import org.bukkit.command.CommandExecutor;

/**
 * Calls for registering {@link CommandExecutor}s are in this class.
 */
public class RegisterGameCmds {

    /**
     * Registers all the commands.
     */
    public static void register() {
        reg(new AchievementsCmd(), "Achievements");
        reg(new LevelCmd(), "Level");
        reg(new Ref(), "Ref");
        reg(new StoreCmd(), "Store", "Buy");
    }
    
    /**
     * Registers a command.
     * @param ce the CommandExecutor class to register the command labels to.
     * @param cmds the command label.
     */
    private static void reg(CommandExecutor ce, String... cmds) {
        for (String cmd : cmds) {
            Games.plugin.getCommand(cmd).setExecutor(ce);
        }
    }

}
