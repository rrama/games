package com.derplin.games.commands;

import com.derplin.games.Games;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * The /Level {@link CommandExecutor}.
 */
public class LevelCmd implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (args.length == 0) {
            if (SenderUtil.isntPlayerMsg(sender)) return false;
            try {
                List<Integer> levelAndXP = Games.getGamesDatabase().getPlayerLevelAndXP(sender.getName());
                sender.sendMessage(ChatColor.YELLOW + "Your current level is " + ChatColor.GOLD + levelAndXP.get(0)
                                   + ChatColor.YELLOW + " and you are " + ChatColor.GOLD
                                   + (xpToNextLevel(levelAndXP.get(0)) - levelAndXP.get(1)) + ChatColor.YELLOW
                                   + " experience points away from the next level.");
            } catch (Exception ex) {
                sender.sendMessage(ChatColor.YELLOW + "An error occurred retrieving your level from the database.");
            }
            return true;
        } else {
            String whom = SenderUtil.getOfflinePlayerName(sender, args[0]);
            if (whom == null) return false;
            try {
                List<Integer> levelAndXP = Games.getGamesDatabase().getPlayerLevelAndXP(whom);
                sender.sendMessage(ChatColor.YELLOW + whom + "'s current level is " + ChatColor.GOLD + levelAndXP.get(0)
                                   + ChatColor.YELLOW + " and they are " + ChatColor.GOLD
                                   + (xpToNextLevel(levelAndXP.get(0)) - levelAndXP.get(1)) + ChatColor.YELLOW
                                   + " experience points away from the next level.");
            } catch (Exception ex) {
                sender.sendMessage(ChatColor.YELLOW + "An error occurred retrieving " + whom + "'s level from the database.");
            }
            return true;
        }
    }
    
    /**
     * Returns the amount of experience needed to level-up to the
     * level specified from the start of the level below it.
     * @param level the level to see how much experience it would take to level-up to it.
     * @return the amount of experience needed to level-up to <code>level</code>
     *         from the start of the level below <code>level</code>.
     */
    public static int xpToNextLevel(int level) {
        return (int)(15 * Math.pow(level + 1, 0.45));
    }
}