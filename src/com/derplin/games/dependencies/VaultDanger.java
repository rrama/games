package com.derplin.games.dependencies;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;

/**
 * Calls to unsafe Vault dependencies go here.
 * Everything throws {@link ClassNotFoundException}.
 */
public class VaultDanger {
    
    /**
     * Returns the economy provider registered with Vault.
     * @return the economy provider registered with Vault.
     */
    private static Economy getEconomy() {
        return Bukkit.getServicesManager().getRegistration(Economy.class).getProvider();
    }
    
    /**
     * Checks if there is an economy provider registered with Vault.
     * @return {@code true} if there is an economy provider registered.
     * @throws ClassNotFoundException if Vault is not found.
     */
    public static boolean canHookEconomy() throws ClassNotFoundException {
        return getEconomy().isEnabled();
    }
    
    /**
     * Gets the balance of a {@link Player}.
     * @param pn the name of the {@link Player}.
     * @return the balance of {@link Player} with name <code>pn</code>.
     * @throws ClassNotFoundException if Vault is not enabled.
     */
    public static double getBalance(String pn) throws ClassNotFoundException {
        return getEconomy().getBalance(pn);
    }
    
    /**
     * Adds money to the balance of a {@link Player}.
     * @param pn the name of the {@link Player} to give money to.
     * @param amount the amount of money to give the {@link Player} with name <code>pn</code>.
     * @return {@code true} if it was able to give <code>pn</code> money.
     * @throws ClassNotFoundException if Vault is not enabled.
     */
    public static boolean giveMoney(String pn, int amount) throws ClassNotFoundException {
        return getEconomy().depositPlayer(pn, amount).transactionSuccess();
    }
    
    /**
     * Takes money from the balance of a {@link Player}.
     * {@link Player}s are unable to go into negative funds.
     * @param pn the name of the {@link Player} to take money from.
     * @param amount the amount of money to take from the {@link Player} with name <code>pn</code>.
     * @return {@code true} if it was able to take money from <code>pn</code>.
     * @throws ClassNotFoundException if Vault is not enabled.
     */
    public static boolean takeMoney(String pn, int amount) throws ClassNotFoundException {
        return getEconomy().withdrawPlayer(pn, amount).transactionSuccess();
    }
    
}