package com.derplin.games;

import com.derplin.games.achievements.RegisterAchievements;
import com.derplin.games.commands.RegisterGameCmds;
import com.derplin.games.events.RegisterGameEvents;
import java.io.File;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main class for the Games library.
 *
 * @author rrama, Lonesface and Arrem
 *         All rights reserved to rrama, Lonesface and Arrem. Use by Derplin only.
 *         No copying/stealing any part of the code (Exceptions; You have written consent from a member of the Derplin development team).
 *         No copying/stealing ideas from the code (Exceptions; You have written consent from a member of the Derplin development team).<br />
 *         <p/>
 *         Credit goes to rrama (author), Lonesface (author), Arrem (author)
 */
public class Games extends JavaPlugin {

    public static Games plugin;
    public static GamePlugin game;
    public static YamlConfiguration mapMeta;
    private static GamesDatabase database;

    @Override
    public void onDisable() {
        Bukkit.getScheduler().cancelTasks(plugin);
        
        database.disconnect();
    }
    
    @Override
    public void onEnable() {
        plugin = this;
        
        FolderSetup.setupFolders();
        database = new GamesDatabase();
        RegisterAchievements.register();
        RegisterGameEvents.register();
        RegisterGameCmds.register();
    }
    
    /**
     * Gets the database for storing levels and {@link Achievement}s.
     * @return the database.
     */
    public static GamesDatabase getGamesDatabase() {
        return database;
    }
    
    /**
     * Awards an {@link Player} experience points.
     * If the {@link Player} has enough experience they will level-up.
     * The experience is stored in a database.
     * @param p  the {@link Player} who is being awarded the experience.
     * @param xp the amount of experience to award the specified {@link Player} with.
     */
    public static void awardXP(Player p, int xp) {
        awardXP(p.getName(), xp);
    }
    
    /**
     * Awards an {@link Player} experience points.
     * If the {@link Player} has enough experience they will level-up.
     * The experience is stored in a database.
     * @param playerName the name of {@link Player} who is being awarded the experience.
     * @param xp         the amount of experience to award the specified {@link Player} with.
     */
    public static void awardXP(String playerName, int xp) {
        database.addPlayerXP(playerName, xp);
    }
    
    /**
     * Loads a map as world_InPlay.
     * Must not be called from the main {@link Thread}!
     * @param callingPlugin the plugin who's data folder the {@link World} is stored in.
     * @param mapName       the name of the {@link World} to load. Must be stored in the {@link Plugin}'s data folder in Worlds.
     * @throws IOException if the map is unable to be copied from the {@link Plugin}'s data folder to the main folder.
     */
    public static void loadMap(Plugin callingPlugin, String mapName) throws IOException {
        File file = new File(callingPlugin.getDataFolder() + FolderSetup.FS + "Worlds", FolderSetup.FS + mapName);
        File copyLoc = new File(Bukkit.getWorldContainer(), FolderSetup.FS + "world");
        if (copyLoc.exists() && !copyLoc.delete()) {
            throw new IOException("Unable to delete previous map folder.");
        }
        Util.copyDirectory(file, copyLoc);
    }

}