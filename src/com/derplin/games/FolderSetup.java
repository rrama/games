package com.derplin.games;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * The class where {@link File}s, folders and config is setup.
 */
public class FolderSetup {
    
    public static final String FS = System.getProperty("file.separator");
    public static final String LSep = System.getProperty("line.separator");
    public static final String GTag = "[Games] ";
    public static final String GTagB = GTag + ChatColor.BLUE;
    protected static File dataFolder, database;
    
    /**
     * Calls all setting up of initial {@link File}s and configs.
     */
    public static void setupFolders() {
        dataFolder = Games.plugin.getDataFolder();
        if (!dataFolder.exists()) {
            if (!dataFolder.mkdir()) {
                Bukkit.getLogger().warning(GTagB + "DataFolder failed to be made.");
            } else {
                Bukkit.getLogger().info(GTagB + "Created a DataFolder for you :). rrama do good?");
            }
            setupFolders();
        } else {
            fileSetup();
            configSetup();
        }
    }
    
    /**
     * Creates all the {@link File}s.
     */
    public static void fileSetup() {
        database = createFile(dataFolder, "Database.db", null);
    }
    
    /**
     * Initialises config and assigns some variables their values.
     */
    public static void configSetup() {
//        FileConfiguration FC = Games.plugin.getConfig();
//        
//        Games.plugin.saveConfig();
//        Games.plugin.reloadConfig();
    }
    
    /**
     * Creates a new folder if it doesn't already exist.
     * @param folder The parent folder.
     * @param folderName The new folder's name.
     * @return The newly created (or old, found) folder.
     */
    public static File createFolder(File folder, String folderName) {
        File newFolder = new File(folder, FS + folderName);
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }
        return newFolder;
    }
    
    /**
     * Creates a new {@link File} if it doesn't already exist and outputs a creation message.
     * @param folder The folder to create the {@link File} in.
     * @param fileName The name of the {@link File} to be created.
     * @param toWrite The original content to be put in the {@link File}. Only used if the {@link File} is new.
     * @return The newly created (or old, found) folder.
     */
    public static File createFile(File folder, String fileName, String toWrite) {
        return createFile(folder, fileName, toWrite, true);
    }
    
    /**
     * Creates a new {@link File} if it doesn't already exist.
     * @param folder The folder to create the {@link File} in.
     * @param fileName The name of the file to be created.
     * @param toWrite The original content of the {@link File}. Only used if the {@link File} is new.
     * @param msg Whether or not an output message on creation should be broadcasted.
     * @return The newly created (or old, found) folder.
     */
    public static File createFile(final File folder, final String fileName, final String toWrite, boolean msg) {
        File file = new File(folder, FS + fileName);
        if (!file.exists()) {
            try {
                if (!file.createNewFile()) {
                    Bukkit.getLogger().warning(GTagB + "File '" + fileName + "' failed to be made.");
                } else {
                    if (toWrite == null) {
                        if (msg) {
                            Bukkit.getLogger().info(GTagB + "Created '" + fileName + "' for you :). rrama do good?");
                        }
                    } else {
                        try (FileWriter fileWriter = new FileWriter(file)) {
                            fileWriter.write(toWrite);
                            if (msg) {
                                Bukkit.getLogger().info(GTagB + "Created '" + fileName + "' for you :). rrama do good?");
                            }
                        } catch (Exception e) {
                            Bukkit.getLogger().warning(GTagB + "Failed to write to '" + fileName + "'.");
                        }
                    }
                }
                return createFile(folder, fileName, toWrite);
            } catch (IOException ex) {
                Bukkit.getLogger().warning(GTagB + "'IOException' File '" + fileName + "' failed to be made in '" + folder + "'.");
                return null;
            }
        } else return file;
    }
}