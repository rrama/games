package com.derplin.games.gameutil;

import com.derplin.games.Games;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

/**
 * Broadcasts a message with the countdown time put in when the time left is
 * devisable by 120 seconds, is 60 seconds, 30 seconds, 10 seconds or 5 or less seconds.
 */
public class CountDown {
    
    private final String dmsgFormat;
    private final Runnable runAfter;
    private BukkitTask task;
    private int timeLeft;
    
    /**
     * The constructor for a {@link CountDown}.
     * Automatically starts the countdown.
     * @param time      the time to countdown from.
     * @param msgFormat the message to display on the intervals.
     *                  The message should include <code>%d</code>.
     * @param after     the {@link Runnable} task to do at 0 seconds.
     */
    public CountDown(int time, String msgFormat, Runnable after) {
        dmsgFormat = msgFormat;
        runAfter = after;
        if (time < 0) return;
        timeLeft = time;
        task = Bukkit.getScheduler().runTaskTimerAsynchronously(Games.plugin, new Runnable() {

            @Override
            public void run() {
                if (timeLeft == 0) {
                    task.cancel();
                    if (runAfter != null) {
                        Bukkit.getScheduler().runTask(Games.plugin, runAfter);
                    }
                    return;
                }
                if (timeLeft % 120 == 0 || timeLeft == 60 || timeLeft == 30 || timeLeft == 10 || timeLeft < 6) {
                    Bukkit.broadcastMessage(String.format(dmsgFormat, timeLeft));
                }
                timeLeft--;
            }
            
        }, 0, 20);
    }
    
    /**
     * Gets the remaining time in seconds of the {@link Countdown}.
     * @return the remaining time.
     */
    public int getTimeLeft() {
        return timeLeft;
    }
    
    /**
     * Cancels the {@link Countdown}.
     */
    public void cancel() {
        if (task == null) return;
        task.cancel();
        task = null;
    }
    
}