package com.derplin.games.gameutil;

import com.derplin.games.Games;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import org.bukkit.Bukkit;

/**
 * An optional manager for the maps of a game.
 */
public class MapManager {
    
    private static final ArrayList<String> maps = new ArrayList<>();
    
    /**
     * Loads the maps in the <code>Worlds</code> folder inside the current games data folder.
     * Must be called after <code>Games#game</code> is set.
     */
    public static void initialiseMaps() {
        initialiseMaps(false);
    }
    
    /**
     * Loads the maps in the <code>Worlds</code> folder inside the current games data folder.
     * Must be called after <code>Games#game</code> is set.
     * @param mustHaveMeta if {@code true} then maps will not be loaded if they do not contain <code>MapMeta.yml</code>.
     */
    public static void initialiseMaps(boolean mustHaveMeta) {
        File worldFolder = new File(Games.game.getDataFolder(), "Worlds");
        worldFolder.mkdirs();
        for (File f : worldFolder.listFiles(new FolderFilter())) {
            if (f.getName().equalsIgnoreCase("Unused")) continue;
            if (mustHaveMeta && !new File(f, "MapMeta.yml").exists()) {
                Bukkit.getLogger().warning("Cannot load  map '" + f.getName() + "' as it has no meta.");
                continue;
            }
            maps.add(f.getName());
        }
    }
    
    /**
     * Gets all the loaded maps that were in the
     * <code>Worlds</code> folder inside the current games data folder.
     * <code>initialiseMaps()</code> must have been called.
     * @return an <code>ArrayList</code> of the names of all the loaded maps.
     */
    public static ArrayList<String> getMaps() {
        return maps;
    }
    
    /**
     * Gets the name of a random map out of all the loaded maps.
     * <code>initialiseMaps()</code> must have been called.
     * @return the name of a loaded map or {@code null} if no maps are loaded.
     */
    public static String getRandomMap() {
        Random rand = new Random();
        try {
            return maps.get(rand.nextInt(maps.size()));
        } catch (Exception ex) {
            return null;
        }
    }
    
}