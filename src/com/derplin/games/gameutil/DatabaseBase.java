package com.derplin.games.gameutil;

import java.io.File;
import org.bukkit.Bukkit;

import java.sql.*;
import java.util.logging.Level;
import org.bukkit.entity.Player;

/**
 * Basis of an SQLite database.
 */
public abstract class DatabaseBase {

    private Connection connection;

    /**
     * Tries to connect to the database.
     * @param file the {@link File} which the database is stored in.
     */
    public DatabaseBase(File file) {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + file.getAbsolutePath());
        } catch (ClassNotFoundException ex) {
            Bukkit.getLogger().warning("Database dependency not found!");
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Disconnects from the database.
     */
    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
        }
    }
    
    /**
     * Updates the column with name <code>record</code> to <code>value</code> for <code>playerName</code>.
     *
     * @param playerName the {@link Player} who's record is being updated.
     * @param table the table to update the record on.
     * @param record     the column name.
     * @param value      the new value.
     * @return {@code true} if the update was successful.
     */
    protected boolean updateRecord(String playerName, String table, String record, String value) {
        if (getPlayerRecord(playerName, table) == null) {
            return false;
        }

        return executeStatement("UPDATE " + table + " SET " + record + "=" + value + " WHERE name='" + playerName + "'");
    }

    /**
     * Returns the {@link ResultSet} for <code>playerName</code> or {@code null} if not found.
     * {@link ResultSet#next()} has already been used.
     *
     * @param playerName the {@link Player}'s name.
     * @param table the table to get the record from.
     * @return the {@link ResultSet} for <code>playerName</code> or {@code null} if not found.
     */
    protected ResultSet getPlayerRecord(String playerName, String table) {
        return getPlayerRecord(playerName, table, false);
    }

    /**
     * Returns the {@link ResultSet} for <code>playerName</code> or {@code null} if not found.
     * {@link ResultSet#next()} has already been used.
     * Use {@link #getPlayerRecord(String)} unless called from {@link #createNewRecord(String, String, boolean)}.
     *
     * @param playerName    the {@link Player}'s name.
     * @param table         the table to get the record from.
     * @param secondAttempt if it has already tried creating a new record.
     * @return the {@link ResultSet} for <code>playerName</code> or {@code null} if not found.
     */
    private ResultSet getPlayerRecord(String playerName, String table, boolean secondAttempt) {
        ResultSet rs = executeQuery("SELECT * FROM " + table + " WHERE name = '" + playerName + "'");
        if (rs == null) {
            return createNewRecord(playerName, table, secondAttempt);
        }
        try {
            if (!rs.next()) {
                return createNewRecord(playerName, table, secondAttempt);
            }
            return rs;
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
            return null;
        }
    }

    /**
     * Creates a new record for the <code>playerName</code>.
     * If it's the <code>secondAttempt</code> it returns {@code null} straight off.
     *
     * @param playerName    the name of the new record.
     * @param table         the table to create the record on.
     * @param secondAttempt if creating a new record has already been attempted.
     * @return {@code null} if <code>secondAttempt</code> else <code>getPlayerRecord(playerName, true)</code>.
     */
    private ResultSet createNewRecord(String playerName, String table, boolean secondAttempt) {
        if (secondAttempt) return null;
        if (!executeStatement("INSERT INTO " + table + " (name) VALUES ('" + playerName + "')")) {
            return null;
        }
        return getPlayerRecord(playerName, table, true);
    }

    /**
     * Executes the SQL code if the database is up.
     *
     * @param sql the SQL code to execute.
     * @return {@code true} if no error executing.
     */
    protected boolean executeStatement(String sql) {
        if (!isUp()) return false;
        try (Statement statement = connection.createStatement()) {
            statement.execute(sql);
            return true;
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
            return false;
        }
    }

    /**
     * Executes the SQL query if the database is up and returns the {@link ResultSet}.
     * If there are any errors {@code null} is returned.
     *
     * @param sql the SQL query to execute.
     * @return the {@link ResultSet} from the query or {@code null}.
     */
    protected ResultSet executeQuery(String sql) {
        if (!isUp()) {
            return null;
        }

        try {
            return connection.prepareStatement(sql).executeQuery();
        } catch (SQLException ex) {
            Bukkit.getLogger().log(Level.WARNING, ex.getLocalizedMessage(), ex);
            return null;
        }
    }

    /**
     * Returns whether the database is connected or not.
     *
     * @return {@code true} if the database is connected.
     */
    public boolean isUp() {
        try {
            return !connection.isClosed() && !connection.isReadOnly();
        } catch (Exception ex) {
            return false;
        }
    }
}